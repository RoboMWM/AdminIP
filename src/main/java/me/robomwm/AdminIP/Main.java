package me.robomwm.AdminIP;

import org.bukkit.BanEntry;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

/**
 * Created by Robo on 2/6/2016.
 */
public class Main extends JavaPlugin implements Listener
{
    @Override
    public void onEnable()
    {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onPlayerLogin(PlayerLoginEvent event)
    {
        if (!event.getHostname().equals("tf.us.to:25565") && !event.getHostname().equals("tf.us.to:22333"))
        {
            //BanList bans = Bukkit.getServer().getBanList(BanList.Type.NAME);
            //bans.addBan(event.getPlayer().getName(), "shush nothing was here", null, "AdminIP");
            //event.disallow(PlayerLoginEvent.Result.KICK_BANNED, "nothin");
            this.getLogger().log(Level.INFO, "Hostname used: " + event.getHostname().toString());
        }
    }
}
